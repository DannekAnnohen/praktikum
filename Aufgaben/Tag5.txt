Tag 5 -------------------------------------------------------------

	-- Wrap-up der ersten Woche --

Da heute die erste Praktikumswoche endet, waere jetzt ein guter Zeitpunkt noch fehlende Punkte
der dieswoechigen Aufgaben zu erfuellen und die erhobenen Daten zusammen zu tragen und in einen
Kontext zu setzen. Ebenfalls wichtig ist es, sich schon einmal Gedanken bzgl der Planung der 
naechsten Woche zu machen. Was fuer Daten koennen noch erhoben werden? Koennen einige der bisher
entwickelten Verfahren noch verfeinert werden? 

Aufgabe 1
Zumindest ein erster Lauf der Datenerhebung der Proteindomains mit HMMER sollte heute 
beendet werden.

Zur Erinnerung, 1) Suche mit der Auswahl(!) von PFAM Domain Models, die fuer
euer Protein charakteristisch sind gegen alle Proteine in den Proteomen. Ziel dabei, eine
alternative zu blastp zu verwenden um gute Hits in mehr Spezies zu finden. 2) Suche mit allen(!)
PFAM Domain Models gegen eure Kandidatenproteine(!) (die mit blastp und HMMER gefundenen
potentiellen Homologen). Ziel dabei, eventuell zusaetzliche/andere Domainen in den
Kandidatenproteinen zu identifizieren.

Selbiges gilt fuer erste Erstellung von Splitstree-Bauemen. BLAST-Ergebnisse
hat hoffentlich jeder vorliegen.

Aufgabe 2
Diese Daten sollten kritisch betrachtet werden, um moegliche Schwaechen in den 
angewendeten Arbeitsprozeduren aufzuspueren und ggf. an eigenen Skripten etc. nachzubessern. 
Fangt an, Eure Daten in einen sinnvollen Kontext zu setzen. Wie unterscheiden sich Splits-
tree erstellte Baeume fuer welche Proteine? Korreliert dies in einer auffaelligen Weise mit
den Ergebnissen der HMMER-Analyse? Wie sieht es im Vergleich der BLAST- und HMMER-Ergebnisse
aus? Existieren vielleicht externe Quellen (Paper etc) die solche Diskrepanzen erklaeren 
koennen? 
Solche Betrachtungen duerfen natuerlich auch gern mit statistischen und grafischen Auswertungen
a la R, Pyplot, Scikit etc unterstuetzt werden.
Investiert etwas Zeit um Euch mit Euren Ergebnissen auseinander zu setzen. Noch einmal -
auffaellig bedeutet nicht unbedingt falsch, unerwartete Ergebnisse koennen auch darauf hindeuten
etwas interessantes gefunden zu haben ;)

Und damit wir am Wochenende auch noch etwas zu tun haben:

Ihr koennt euch schon ansehen, wie der finale Report in schriftlicher Form (Format: PDF oder
HTML(oder was ein normaler Internet-browser oeffnen kann)) aussehen soll.

Aufgabe x
Ihr koennt euch schon ansehen, wie der finale Report in schriftlicher Form (Format: PDF oder
HTML(oder was ein normaler Internet-browser oeffnen kann)) aussehen soll, und zwar
unter Aufgaben in "guidelines_final_report.txt".

Spatestens jetzt solltet Ihr Euch noch einmal die Zeit nehmen, Eure reports in eine lesbare Form 
zu bringen, da wir dort bestimmt noch einmal drueber schauen werden. Es sollte zumindest 
ersichtlich sein:

-Welche Aufgaben bearbeitet wurden, was dabei herausgekommen ist
-ein genereller Ueberblick, in welcher Anordnung die Arbeitsschritte unternommen wurden
-Hat sich Euer Wissen ueber die betrachteten Proteine erweitert? Habt Ihr vielleicht 
 eine Hypothese die einige Eurer Ergebnisse erklaeren koennte?
-Noch bestehende Probleme...
-und vielleicht ein paar kurze Saetze, die das weitere geplante Vorgehen beschreiben



