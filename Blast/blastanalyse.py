import xml.etree.ElementTree as ET
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
import sys
import re
import math


def main():
    filename=sys.argv[1]

    if filename[3] == '1' or filename[3] == '0': 
        sanitizedname = " ".join(filename.split("_")[1:])
        newname = sanitizedname.replace("SIN3protein sc.fasta.QUERY", 'SIN3').replace('protein.fasta.QUERY', '').replace('.proteins.fasta', '_')
    else:
        newname = filename[3:].replace("protein_sc.fasta.QUERY", '').replace(".faa", '').replace("protein.fasta.QUERY", '')
        print(newname)
    tree = ET.parse(sys.argv[1])
    root = tree.getroot()
    hit =0
    for iteration in root.iter('Iteration'):
        transcript = iteration.find('Iteration_query-def').text
        for hit in iteration.iter('Hit'):
            hit = 1
            for hsp in iteration.iter('Hsp'):

                evalue = hsp.find('Hsp_evalue').text
                if float(evalue) > 10e-5:
                    with open(newname+".EVALFAILURE", "w") as output_handle:
                        output_handle.write("E-Value too low at: "+str(evalue))
                    break
                score = hsp.find('Hsp_score').text
                seq = hsp.find('Hsp_hseq').text
                id="e-value="+evalue+" score="+score
                record = SeqRecord(Seq(seq, IUPAC.protein), id= id, name=transcript, description= newname)
                with open(newname+".fasta", "w") as output_handle:
                    SeqIO.write(record, output_handle, "fasta")
                break
            break
        break #unbreak for all isoforms

    if hit == 0:
        with open(newname+".HITFAILURE", "w") as output_handle:
            output_handle.write("No hits found")

if __name__ == "__main__":
    main()
