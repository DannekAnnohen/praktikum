import sys


files = ["HDAC1_all.fasta", "HDAC2_all.fasta", "RBBP4_all.fasta", "RBBP7_all.fasta", "SAP18_all.fasta", "SAP30_all.fasta", "SIN3_all.fasta"]

namedict = {
    "Acanthoeca_spectabilis" : "ASPE_Acanthoma_spectabilis_CHO",
    "AMIL" : "AMIL_Acropora_millepora_CNI",
    "AQUE" : "AQUE_Amphimedon_queenslandica_POR",
    "ATEN" : "ATEN_Actinia_tenebrosa_CNI",
    "ATHA" : "ATHA_Arabidopsis_thaliana_VIR",
    "Choanoeca_perplexa" : "CPER_Choanoeca_perplexa_CHO",
    "Codosiga_hollandica" : "CHOL_Codosiga_hollandica_CHO",
    "COWC" : "COWC_Capsaspora_owczarzaki_FIL",
    "DGIG" : "DGIG_Dendronephthya_gigantea_CNI",
    "Diaphanoeca_grandis" : "DGRA_Diaphanoeca_grandis_CHO",
    "Didymoeca_costata" : "DCOS_Didymoeca_costata_CHO",
    "DMEL" : "DMEL_Drosophila_melanogaster_BIL",
    "EPAL" : "EPAL_Exaiptasia_pallida_CNI",
    "Hartaetosiga_balthica" : "HBAL_Hartaetosiga_balthica_CHO",
    "Hartaetosiga_gracilis" : "HGRA_Hartaetosiga_gracilis_CHO",
    "Helgoeca_nana" : "HNAN_Helgoeca_nana_CHO",
    "HVUL" : "HVUL_Hydra_vulgaris_CNI",
    "MBRE" : "MBRE_Monosiga_brevicollis_CHO",
    "Microstomoeca_roanoka" : "MROA_Microstomoeca_roanoka_CHO",
    "MSQU" : "MSQU_Myxobolus_squamalis_CNI",
    "Mylnosiga_fluctuans" : "MFLU_Mytnosiga_fluctuans_CHO",
    "NVEC" : "NVEC_Nematostella_vectensis_CNI",
    "OFAV" : "OFAV_Orbicella_faveolata_CNI",
    "PDAM" : "PDAM_Pocillopora_damicornis_CNI",
    "Salpingoeca_dolichothecata" : "SDOL_Salpingoeca_dolichothecata_CHO",
    "Salpingoeca_helianthica" : "SHEL_Salpinogaeca_helianthica_CHO",
    "Salpingoeca_infusionum" : "SINF_Salpingoeca_infusionum_CHO",
    "Salpingoeca_kvevrii" : "SKVE_Salpinogoeca_kvevrii_CHO",
    "Salpingoeca_macrocollata" : "SMAC_Salpinogaeca_macrocollata_CHO",
    "Salpingoeca_punica" : "SPUN_Salpinogaeca_punica_CHO",
    "Salpingoeca_urceolata" : "SURC_Salpinogaeca_urceloata_CHO",
    "SARC" : "SARC_Sphaeroforma_arctica_TER",
    "Savillea_parva" : "SPAR_Savillea_parva_CHO",
    "SCER" : "SCER_Saccharomyces_cerevisiae_FUN",
    "SHEL" : "SHEL_Salpinogaeca_helianthica_CHO",
    "SPIS" : "SPIS_Stylophora_pistillata_CNI",
    "SROS" : "SROS_Salpinogoeca_rosetta_CHO",
    "Stephanoeca_diplocostata.Australia" : "SDIP_Stephanoeca_diplocostata_Australia_CHO",
    "Stephanoeca_diplocostata.France" : "SDIP_Stephanoeca_diplocostata_France_CHO",
    "TADH" : "TADH_Myxobolussquamalis_PLA",
    "TKIT" : "TKIT_Trichoplax_adherens_CNI",
    "TSPH" : "TSPH_Trichoplax_spH2_PLA",

    "Acanthoeca spectabilis" : "ASPE_Acanthoma_spectabilis_CHO",
    "Choanoeca perplexa" : "CPER_Choanoeca_perplexa_CHO",
    "Codosiga hollandica" : "CHOL_Codosiga_hollandica_CHO",
    "Diaphanoeca grandis" : "DGRA_Diaphanoeca_grandis_CHO",
    "Didymoeca costata" : "DCOS_Didymoeca_costata_CHO",
    "Hartaetosiga balthica" : "HBAL_Hartaetosiga_balthica_CHO",
    "Hartaetosiga gracilis" : "HGRA_Hartaetosiga_gracilis_CHO",
    "Helgoeca nana" : "HNAN_Helgoeca_nana_CHO",
    "Microstomoeca roanoka" : "MROA_Microstomoeca_roanoka_CHO",
    "Mylnosiga fluctuans" : "MFLU_Mytnosiga_fluctuans_CHO",
    "Salpingoeca dolichothecata" : "SDOL_Salpingoeca_dolichothecata_CHO",
    "Salpingoeca helianthica" : "SHEL_Salpinogaeca_helianthica_CHO",
    "Salpingoeca infusionum" : "SINF_Salpingoeca_infusionum_CHO",
    "Salpingoeca kvevrii" : "SKVE_Salpinogoeca_kvevrii_CHO",
    "Salpingoeca macrocollata" : "SMAC_Salpinogaeca_macrocollata_CHO",
    "Salpingoeca punica" : "SPUN_Salpinogaeca_punica_CHO",
    "Salpingoeca urceolata" : "SURC_Salpinogaeca_urceloata_CHO",
    "Savillea parva" : "SPAR_Savillea_parva_CHO",
    "Stephanoeca diplocostata.Australia" : "SDIP_Stephanoeca_diplocostata_Australia_CHO",
    "Stephanoeca diplocostata.France" : "SDIP_Stephanoeca_diplocostata_France_CHO"
    }

def main():
    for file in files:
        with open(file.replace("_all.fasta", "_candidates.fasta"), "w+") as newfile:
            with open(file) as currentfile:
                lines = currentfile.readlines()
                for l in lines:
                    if l[0] == ">":
                        templ = l.split(" ")[2:]
                        newl = ((" ".join(templ)).split("_"))[:-1]
                        final = namedict.get(newl[0])
                        newfile.write(">"+final+"\n")
                    else:
                        newfile.write(l)




if __name__ == "__main__":
    main()
