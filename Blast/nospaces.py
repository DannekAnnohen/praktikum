import sys


def main():
    filename=sys.argv[1]
    with open(filename) as currentfile:
        lines = currentfile.readlines()
        with open(filename.replace("_san.fasta", "_nospace.fasta"), "w+") as newfile:
            #print(lines)
            for l in lines:
                if l[0] == ">":

                    templ = l.replace(" ", "_")
                    newfile.write(">"+templ)
                else:
                    newl = l
                    newfile.write(newl)




if __name__ == "__main__":
    main()
