import sys





def main():
    filename=sys.argv[1]
    with open(filename) as currentfile:
        lines = currentfile.readlines()
        with open(filename.replace(".fasta", "_san.fasta"), "w+") as newfile:
            #print(lines)
            for l in lines:
                if l[0] == ">":

                    templ = l.split(" ")[2:]
                    newl = ((" ".join(templ)).split("_"))[:-1]
                    final = newl[0]
                    newfile.write(">"+final+"\n")
                else:
                    newl = l
                    newfile.write(newl)




if __name__ == "__main__":
    main()
