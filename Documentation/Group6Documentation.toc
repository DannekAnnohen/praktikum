\babel@toc {english}{}
\contentsline {section}{\numberline {1}Proteins}{2}
\contentsline {subsection}{\numberline {1.1}Overview}{2}
\contentsline {subsection}{\numberline {1.2}HDAC1 and HDAC2}{2}
\contentsline {subsection}{\numberline {1.3}RBBP4 and RBBP7}{2}
\contentsline {subsection}{\numberline {1.4}SAP30}{3}
\contentsline {subsection}{\numberline {1.5}SAP18}{3}
\contentsline {subsection}{\numberline {1.6}Sin3}{4}
\contentsline {section}{\numberline {2}Tree Creation}{5}
\contentsline {subsection}{\numberline {2.1}Blast}{5}
\contentsline {subsection}{\numberline {2.2}HMMER}{5}
\contentsline {subsection}{\numberline {2.3}SplitsTree}{5}
\contentsline {section}{\numberline {3}Interpretion of Trees}{7}
\contentsline {subsection}{\numberline {3.1}MetazoaOri\_topo\_tree.png}{7}
\contentsline {subsection}{\numberline {3.2}HDAC1}{7}
\contentsline {subsection}{\numberline {3.3}HDAC2}{7}
\contentsline {subsection}{\numberline {3.4}RBBP4}{7}
\contentsline {subsection}{\numberline {3.5}RBBP7}{7}
\contentsline {subsection}{\numberline {3.6}SAP18}{7}
\contentsline {subsection}{\numberline {3.7}SAP30}{7}
\contentsline {subsection}{\numberline {3.8}Sin3}{7}
