Bitte schreibt in diese Datei am Ende des Tages was ihr alles gemacht habt, einerseits für euch, damit damit ihr ein überblich habt eas ihr alles geschafft habt, andererseits für die Tutoren, damit wir wissen wo ihr jeweils grade steht.

Diese Datei soll also am Ende jeden Tages committed und gepushed werden.

In dem report sollten stehen: 
* ein Datum
* Erkenntnisse und was ihr gelernt habt, insbesondere über eure Proteine.
* Verweise auf Graphiken und Bäume die ihr erstellt habt.
* Offene Probleme sowie  Aufgaben und Ideen wie ihr damit weitermachen werdet.
* aus dem protoll darf gerne hervorgehen wer an was gearbeitet hat, insbesondere dann, wenn ihr das arbeitsergebnis verschiedener Personen  über einen account committed. 

22.06.2020 Montag

- Installieren und Einrichten von Linux und benötigter Software
- Protein Recherche, Begriffe googlen, Grundlagenwissen aneignen

23.06.2020 Dienstag

- Installieren und Einrichten von Linux und benötigter Software
- Recherche zu den uns zugeteilten Proteinen 
- = Tag 1 Aufgaben 1 und 2
- nicht mehr geschafft wegen anderen Aufgaben des Lebens und Internetproblemen:
	https://windows-love.de/2020/06/23/aktuelle-stoerung-vodafone-glasfaserschaden-in-leipzig-internet-telefon-tv-23-06-2020/

25.06.2020 Donnerstag

- Axel: mit Splitstree auseinandersetzen, v.a. bzgl des Inputformats
- Lena: Recherche zu den uns zugeteilten Proteinen fortführen, Dokumentation in Form bringen
- Max: blasten und dokumentieren

25.06.2020 Freitag

- Axel: habe mich weiter mit Splitstree beschäftigt. Habe herausgefunden dass Bäume über MSA kreiert werden. Außerdem habe ich mich damit auseinadner-
	gesetzt, wie man Bäume liest bzw. dass man mehrere Bäume braucht um zuverlässige Ergebnisse bekommen zu können. Die Ergebnisse habe ich 
	relativ unstrukturiert in splitstree.txt festgehalten. Warnung: Do not open
- Lena: Einlesen in hmmer
- Max: weiter blasten

29.06.2020 Montag

- Axel: Recherche Bäume, Netze, Rechtecke?! Richtige fasta Formatierung finden die Splitstree akzeptiert
- Lena: leider krank
- Max: Multiples Alginment mit Clustalo herstellen + mit Axel richtige Formatierung für Splitstree finden & Files korrigieren

30.06.2020 Dienstag

- Lena: Ergebnisse von Splitstree mit MetazoaOri_topo_tree.png vergleichen
- Axel: Skript geschrieben für gescheite und einheitliche Namensgebung der Blastergebnisse (rename.py)

01.07.2020 Mittwoch

- Axel: Auseinandersetzung mit hmmsearch. Script geschrieben für erstellung von Domaintables (Hmmsearch von Proteindomänen gegen Blastergebnisse)
- Lena: ?

02.07.2020 Donnerstag

- Axel: Zäher Kampf mit hmmsearch. Nach etlichen versuchen hat sich herausgestellt, dass hmmsearch -A auf Version 3.2.1 mit gaps umgehen kann und
  3.3 nicht. Ansonsten habe ich ein Script geschrieben um mit esl-reformat ein MVA zu erstellen

03.07.2020 Freitag

- Axel: Weitere Probleme mit hmmsearch -A. Nach einem weiteren Kampf hat sich herausgestellt, dass os.popen den hmmsearch (bei mir!?) nicht korrekt auführen kann. Aufarbeitung der Datenausgabe von esl-reformat für Splitstree. Anwendung auf alle Proteome für SIN3.
