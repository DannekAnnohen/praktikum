import os


# our proteins - noch ergaenzen!
proteins = ["SIN3"]

for p in proteins:


    os.makedirs(
        './hmmer_search_results/' + p, exist_ok=True
    )

    #get all domains of protein p
    DOMAINS = os.listdir("domains/" + p)

    for domain in DOMAINS:
        domainFolder = domain.split(".")[0]

        # Open the file as read
        f = open('./alignments_final/' + p + '/' + domainFolder + '/finalalignment.fasta', "r+")
        # Create an array to hold write data
        new_file = []
        # Loop the file line by line
        for line in f:
          # Split A,B on , and use first position [0], aka A, then add to the new array
          clean1 = line.split("[")[0]
          clean = clean1.replace("/","-")
          # Add
          new_file.append(clean)
        # Open the file as Write, loop the new array and write with a newline
        with open('./alignments_final/' + p + '/' + domainFolder + '/cleanAlignment.fasta', "w+") as f:
          for i in new_file:
            f.write(i+"\n")
