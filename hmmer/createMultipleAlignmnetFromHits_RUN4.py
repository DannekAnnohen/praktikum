import pickle
import os
import pandas

proteins = ["SIN3","SAP30","HDAC1","HDAC2","SAP18","RBBP4"]

for p in proteins:

    #get all domains of protein p
    domains = os.listdir("domains/" + p)

    #create index for protein -> for esl-sfetch
    print("indextest1")
    indexCommand = "esl-sfetch --index " + "./blastresults_for_hmmsearch/" + p + ".fasta"
    print("indextest2")
    print(indexCommand)
    os.system(
        indexCommand
    )

    for d in domains:


        domainHits = []
        domainFolder = d.split('.')[0]

        os.makedirs(
            './alignments_final/' + p + '/' + domainFolder + '/', exist_ok=True
        )


        print("START ############# domain " + domainFolder)
        with open('./hmmer_search_results/' + p + '/' + domainFolder + '/' + "filteredHits.pkl", "rb") as f:
            hits = pickle.load(f)

        resultF = open('./alignments_final/' + p + '/' + domainFolder + '/' + "result.fasta", "w").close()
        resultF = open('./alignments_final/' + p + '/' + domainFolder + '/' + "result.fasta", "a")

        print(hits.empty)
        print(hits.columns.values)

        if not hits.empty:
            targets = hits.index.get_level_values('target').tolist()

            #print(targets)
            print(type(targets))

            for organism in targets:

                fetchCommand = "esl-sfetch ./blastresults_for_hmmsearch/" + p + ".fasta " + organism + " > tmpAl.txt"
                print(fetchCommand)
                os.system(fetchCommand)
                with open('./tmpAl.txt') as f:
                    data = f.read()
                    resultF.write(data)

            resultF.close()
            print("BEFORE CLUSTALO ############# domain " + domainFolder)
            # with open('./alignments_final/' + p + '/' + domainFolder + '/' + "result.fasta", "r") as f:
            clustaloCommand = "clustalo -i " + './alignments_final/' + p + '/' + domainFolder + '/' \
                              + "result.fasta" + " -t Protein -o ./alignments_final/" \
                              + p + "/" + domainFolder + "/mva.fasta --force"
            print('try: ' + clustaloCommand)
            os.system(clustaloCommand)
            print("AFTER CLUSTALO ############# domain " + domainFolder)
        print("END ############# domain " + domainFolder)
