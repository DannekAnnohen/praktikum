import os
import pickle
import pandas

proteins = ["SIN3","SAP30","HDAC1","HDAC2","SAP18","RBBP4"]

for p in proteins:

    #get all domains of protein p
    domains = os.listdir("domains/" + p)

    for d in domains:
        domainHits = []
        domainFolder = d.split('.')[0]

        with open('./hmmer_search_results/' + p + '/' + domainFolder + '/' + 'strippedDomResults.pkl', "rb") as f:
            hits = pickle.load(f)

        #print(hits)
        print("debug1")

        # make converatble
        hits["acc"] = hits["acc"].str.replace("-","")

        print(hits)
        print(hits.columns.values)

        hits = hits.astype({"dom_score":float, "acc":float, "dom_bias":float, "dom_i_eval":float, "dom_c_eval":float, "seq_eval":float})

    # set thresholds: filter all hits that do not fit the criteria (eddylab.org)
        hits = hits[
            (hits.dom_i_eval < 1)
            & (hits.dom_c_eval < 1)
            & (hits.dom_score > hits.dom_bias)
            & (hits.seq_eval < 10e-3)
        ]


        with open('./hmmer_search_results/' + p + '/' + domainFolder + '/' + "filteredHits.pkl", "wb") as f:
            pickle.dump(hits, f)


