#! /bin/env/python
import os
import subprocess

"""
For all Domain HMM search against BLAST RESULT of according Protein
!!!!!!!!!!!!!CAUTION: os.system(hmmsearch...) only works sometimes! doenst work with os.popen at all! 
RUN THIS SCRIPT MULTIPLE TIMES!!!!!!!!!!!!!!!!!!!!!
"""


#cmd command needed:
#hmmsearch --domtblout ./domtables/dtbl PAH.hmm SIN3_all.fasta > out/hmm.out

#our proteins - noch ergaenzen!
proteins = ["SIN3","SAP30","HDAC1","HDAC2","SAP18","RBBP4"]

#list all entries in folder Domains

for p in proteins:

    print("################ protein: " + p)

    os.makedirs(
        './hmmer_search_results/' + p, exist_ok=True
    )

    #get all domains of protein p
    domains = os.listdir("domains/" + p)

    print("####domains: ")
    print('[%s]' % ', '.join(map(str, domains)))

    # remove all '-' from BLAST output - if aligned sequences are used. which doesnt make a lot of sense thoug
    #os.popen(
    #    'cat ./blastresults_for_hmmsearch/' + p + '.fasta | sed "s/-//g; /^$/d" > ./blastresults_for_hmmsearch/' + p + '_nogaps.fasta'
    #)

    # remove all *
    #os.system(
    #    'cat ./blastresults_for_hmmsearch/' + p + '_nogaps.fasta | sed "s/#.*//" > ./blastresults_for_hmmsearch/' + p + '_nogaps.fasta'
    #)

    for domain in domains:
        domainFolder = domain.split(".")[0]
        print("###############DOMAINFOLDER#################")
        print(domainFolder)
        os.makedirs(
            "./hmmer_search_results/" + p + "/" + domainFolder, exist_ok=True
        )

       # hmmSearchCommand = ("hmmsearch --notextw -A"
       #                     # OUTPUT1: save alignment to
       #                     + " ./hmmer_search_results/" + p + "/" + domainFolder + "/alignment.sto"
       #                     # INPUT 1: get hmmodel of domain from..
       #                     + " ./domains/" + p + "/" + domain
       #                     # INPUT 2: get BLAST result form..
       #                     + " ./blastresults_for_hmmsearch/" + p + "_nogaps.fasta")
       #

        hmmSearchCommandAll = (
                "hmmsearch --notextw"
                # --notextw -A "
                # OUTPUT 1: best hits as sequences
                #+ " ./hmmer_search_results/" + p + "/" + domainFolder + "/alignment.sto"
                # OUTPUT 2: hit table:  simple tabular (space-delimited) file summarizing theper-target
                #           output, with one data line per homologous targetsequence found
                #  + " --tblout "
                #  + " ./hmmer_search_results/" + p + "/" + domainFolder + "/hits.tbl"
                # OUTPUT 3: domain hit table:  simple tabular (space-delimited) file summarizingthe per-domain
                #           output, with one data line per homologousdomain detected in a query sequence for each homologousmodel.
                + " --domtblout "
                + " ./hmmer_search_results/" + p + "/" + domainFolder + "/domHits.dtbl"
                # INPUT 1: get hmmodel of domain from..
                + " ./domains/" + p + "/" + domain
                # INPUT 2: get BLAST result form..
                + " ./blastresults_for_hmmsearch/" + p + ".fasta"
                + " >"
                # OUTPUT 4: console result as output
                + "./hmmer_search_results/" + p + "/" + domainFolder + "/cmd.out"
        )

        # VERSION1: Ergebnis: Multiple Alignmnet in ouputfile1
        # Muster:  hmmsearch -A  outpufile1.sto inputfile1.hmm inputfile2.fasta
        # Example: hmmsearch -A result.sto Sin3a_C.hmm SIN3.fasta
        # os.popen(' source ~/.bash_profile; )
        #res = os.popen(
        #    hmmSearchCommand
        #)

        os.system(hmmSearchCommandAll)


