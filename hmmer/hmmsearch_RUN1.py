#! /bin/env/python
import os
import subprocess

"""
For all Domain HMM search against BLAST RESULT of according Protein
!!!!!!!!!!!!!CAUTION: os.system(hmmsearch...) only works sometimes! doenst work with os.popen at all! 
RUN THIS SCRIPT MULTIPLE TIMES!!!!!!!!!!!!!!!!!!!!!
"""


#cmd command needed:
#hmmsearch --domtblout ./domtables/dtbl PAH.hmm SIN3_all.fasta > out/hmm.out

#our proteins - noch ergaenzen!
proteins = ["SIN3"]

#list all entries in folder Domains

for p in proteins:

    os.makedirs(
        './hmmer_search_results/' + p, exist_ok=True
    )

    #get all domains of protein p
    DOMAINS = os.listdir("domains/" + p)

    # remove all '-' from BLAST output
    os.popen(
        'cat ./blastresults_for_hmmsearch/' + p + '_aligned_candidates.fasta | sed "s/-//g; /^$/d" > ./blastresults_for_hmmsearch/' + p + '_aligned_candidates_nogaps.fasta'
    )

    for domain in DOMAINS:
        domainFolder = domain.split(".")[0]
        os.makedirs(
            "./hmmer_search_results/" + p + "/" + domainFolder, exist_ok=True
        )

        hmmSearchCommand = ("hmmsearch --notextw -A"
        # OUTPUT1: save alignment to
        + " ./hmmer_search_results/" + p + "/" + domainFolder + "/alignment.sto"
        # INPUT 1: get hmmodel of domain from..
        + " ./domains/" + p + "/" + domain
        # INPUT 2: get BLAST result form..
        + " ./blastresults_for_hmmsearch/" + p + "_aligned_candidates_nogaps.fasta")


        hmmSearchCommandAll = (
                "hmmsearch --notextw"
                # --notextw -A "
                # OUTPUT 1: best hits as sequences
                #+ " ./hmmer_search_results/" + p + "/" + domainFolder + "/alignment.sto"
                # OUTPUT 2: hit table:  simple tabular (space-delimited) file summarizing theper-target
                #           output, with one data line per homologous targetsequence found
              #  + " --tblout "
              #  + " ./hmmer_search_results/" + p + "/" + domainFolder + "/hits.tbl"
                # OUTPUT 3: domain hit table:  simple tabular (space-delimited) file summarizingthe per-domain
                #           output, with one data line per homologousdomain detected in a query sequence for each homologousmodel.
                + " --domtblout "
                + " ./hmmer_search_results/" + p + "/" + domainFolder + "/domHits.dtbl"
                # INPUT 1: get hmmodel of domain from..
                + " ./domains/" + p + "/" + domain
                # INPUT 2: get BLAST result form..
                + " ./blastresults_for_hmmsearch/" + p + "_aligned_candidates_nogaps.fasta"
            #    + " >"
                # OUTPUT 4: console result as output
             #   + "./hmmer_search_results/" + p + "/" + domainFolder + "/cmd.out"
        )

        #hmmSearchCommandTest = "/usr/bin/hmmsearch -A ./hmmer_search_results/SIN3/PAH/alignment.sto ./domains/SIN3/PAH.hmm ./blastresults_for_hmmsearch/SIN3_aligned_candidates_nogaps.fasta"


        print(hmmSearchCommand)

        # VERSION1: Ergebnis: Multiple Alignmnet in ouputfile1
        # Muster:  hmmsearch -A  outpufile1.sto inputfile1.hmm inputfile2.fasta
        # Example: hmmsearch -A result.sto Sin3a_C.hmm SIN3_aligned_candidates_nogaps.fasta
        # os.popen(' source ~/.bash_profile; )
        #res = os.popen(
        #    hmmSearchCommand
        #)

        os.system(hmmSearchCommandAll)

        '''
        # VERSION2: Ergebnis: Domaintable in outputfile1, Rated results in outputfile2
        # Muster: hmmsearch --domtblout ouputfile1 inputHmmodel inputBlastResult > outpufile2.out
        res = os.popen(
            "hmmsearch --domtblout "      #  "./domtables/dtbl PAH.hmm SIN3_all.fasta > out/hmm.out"
            # OUTPUT1: save domain table to..
            + "./hmmer_search_results/" + p + "/" + domainFolder + "/domtbl"
            # OUTPUT2: create multiple alignment directly (create one later from the domtable to compare as well)
            # + " --tformat afa"
            # + " -A"
            # + " ./hmmer_search_results/" + p + "/" + domainFolder + "/multplal.sto"
            # INPUT 1: get hmmodel of domain from..
            + " ./domains/" + p + "/" + domain
            # INPUT 2: get BLAST result form..
            + " ./blastresults_for_hmmsearch/" + p + "_aligned_candidates.fasta"
            # OUTPUT3: set output for .out (not sure if needed yet)
            + " >"
            + " ./hmmer_search_results/" + p + "/" + domainFolder + "/hmmsearch.out"
        )
        '''



