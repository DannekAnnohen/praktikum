import os
import pickle
import pandas

proteins = ["SIN3","SAP30","HDAC1","HDAC2","SAP18","RBBP4"]

columns = [
    "target",
    "t_accession",
    "tlen",
    "domainName",
    "q_accession",
    "qlen",
    "seq_eval",
    "seq_score",
    "seq_bias",
    "#",
    "of",
    "dom_c_eval",
    "dom_i_eval",
    "dom_score",
    "dom_bias",
    "hmfrom",
    "hmto",
    "alifrom",
    "alito",
    "envfrom",
    "envto",
    "acc",
]

for p in proteins:

    os.makedirs(
        './hmmer_search_results/' + p, exist_ok=True
    )

    #get all domains of protein p
    domains = os.listdir("domains/" + p)



    for d in domains:
        domainHits = []
        domainFolder = d.split('.')[0]
        print("###############DOMAINFOLDER#################")
        print(domainFolder)
        path = './hmmer_search_results/' + p + '/' + domainFolder + '/domHits.dtbl'
        print(path)
        with open (path) as f :
            for line in f:
                if not line.startswith("#"):
                    fields = line.strip().split(maxsplit=21)
                    domainHits.append(fields)

        tbl = pandas.DataFrame(domainHits, columns=columns)

        # get best hit per organism
        if domainHits:

            tbl = tbl.groupby(["target"], sort=False).max()[
                [
                    "dom_score",
                    "dom_bias",
                    "dom_i_eval",
                    "dom_c_eval",
                    "seq_eval",
                    "qlen",
                    "tlen",
                    "alifrom",
                    "alito",
                    "hmfrom",
                    "hmto",
                    "envfrom",
                    "envto",
                    "acc",
                    "t_accession",
                    "q_accession",
                ]
            ]


            #print("#############TABEL###############")
            #print(tbl)
            #print("debug2")
            #print(type(tbl))
            #print("columns: " + tbl.columns.values)

        print("###############TRY TO SAFE DOMAIN FILE " + domainFolder + "#################")
        with open('./hmmer_search_results/' + p + '/' + domainFolder + '/' + 'strippedDomResults.pkl', "wb") as f :
            pickle.dump(tbl, f)
            print("###############SAFED DOMAIN FILE " + domainFolder + "#################")











