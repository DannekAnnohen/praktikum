import xml.etree.ElementTree as ET
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
import sys
import re
import math
import os.path

namedict = {
    "Acanthoeca_spectabilis" : "ASPE_Acanthoma_spectabilis_CHO",
    "AMIL" : "AMIL_Acropora_millepora_CNI",
    "AQUE" : "AQUE_Amphimedon_queenslandica_POR",
    "ATEN" : "ATEN_Actinia_tenebrosa_CNI",
    "ATHA" : "ATHA_Arabidopsis_thaliana_VIR",
    "Choanoeca_perplexa" : "CPER_Choanoeca_perplexa_CHO",
    "Codosiga_hollandica" : "CHOL_Codosiga_hollandica_CHO",
    "COWC" : "COWC_Capsaspora_owczarzaki_FIL",
    "DGIG" : "DGIG_Dendronephthya_gigantea_CNI",
    "Diaphanoeca_grandis" : "DGRA_Diaphanoeca_grandis_CHO",
    "Didymoeca_costata" : "DCOS_Didymoeca_costata_CHO",
    "DMEL" : "DMEL_Drosophila_melanogaster_BIL",
    "EPAL" : "EPAL_Exaiptasia_pallida_CNI",
    "Hartaetosiga_balthica" : "HBAL_Hartaetosiga_balthica_CHO",
    "Hartaetosiga_gracilis" : "HGRA_Hartaetosiga_gracilis_CHO",
    "Helgoeca_nana" : "HNAN_Helgoeca_nana_CHO",
    "HVUL" : "HVUL_Hydra_vulgaris_CNI",
    "MBRE" : "MBRE_Monosiga_brevicollis_CHO",
    "Microstomoeca_roanoka" : "MROA_Microstomoeca_roanoka_CHO",
    "MSQU" : "MSQU_Myxobolus_squamalis_CNI",
    "Mylnosiga_fluctuans" : "MFLU_Mytnosiga_fluctuans_CHO",
    "NVEC" : "NVEC_Nematostella_vectensis_CNI",
    "OFAV" : "OFAV_Orbicella_faveolata_CNI",
    "PDAM" : "PDAM_Pocillopora_damicornis_CNI",
    "Salpingoeca_dolichothecata" : "SDOL_Salpingoeca_dolichothecata_CHO",
    "Salpingoeca_helianthica" : "SHEL_Salpinogaeca_helianthica_CHO",
    "Salpingoeca_infusionum" : "SINF_Salpingoeca_infusionum_CHO",
    "Salpingoeca_kvevrii" : "SKVE_Salpinogoeca_kvevrii_CHO",
    "Salpingoeca_macrocollata" : "SMAC_Salpinogaeca_macrocollata_CHO",
    "Salpingoeca_punica" : "SPUN_Salpinogaeca_punica_CHO",
    "Salpingoeca_urceolata" : "SURC_Salpinogaeca_urceloata_CHO",
    "SARC" : "SARC_Sphaeroforma_arctica_TER",
    "Savillea_parva" : "SPAR_Savillea_parva_CHO",
    "SCER" : "SCER_Saccharomyces_cerevisiae_FUN",
    "SHEL" : "SHEL_Salpinogaeca_helianthica_CHO",
    "SPIS" : "SPIS_Stylophora_pistillata_CNI",
    "SROS" : "SROS_Salpinogoeca_rosetta_CHO",
    "Stephanoeca_diplocostata.Australia" : "SDIP_Stephanoeca_diplocostata_Australia_CHO",
    "Stephanoeca_diplocostata.France" : "SDIP_Stephanoeca_diplocostata_France_CHO",
    "TADH" : "TADH_Myxobolussquamalis_PLA",
    "TKIT" : "TKIT_Trichoplax_adherens_CNI",
    "TSPH" : "TSPH_Trichoplax_spH2_PLA",
    "Acanthoeca spectabilis" : "ASPE_Acanthoma_spectabilis_CHO",
    "Choanoeca perplexa" : "CPER_Choanoeca_perplexa_CHO",
    "Codosiga hollandica" : "CHOL_Codosiga_hollandica_CHO",
    "Diaphanoeca grandis" : "DGRA_Diaphanoeca_grandis_CHO",
    "Didymoeca costata" : "DCOS_Didymoeca_costata_CHO",
    "Hartaetosiga balthica" : "HBAL_Hartaetosiga_balthica_CHO",
    "Hartaetosiga gracilis" : "HGRA_Hartaetosiga_gracilis_CHO",
    "Helgoeca nana" : "HNAN_Helgoeca_nana_CHO",
    "Microstomoeca roanoka" : "MROA_Microstomoeca_roanoka_CHO",
    "Mylnosiga fluctuans" : "MFLU_Mytnosiga_fluctuans_CHO",
    "Salpingoeca dolichothecata" : "SDOL_Salpingoeca_dolichothecata_CHO",
    "Salpingoeca helianthica" : "SHEL_Salpinogaeca_helianthica_CHO",
    "Salpingoeca infusionum" : "SINF_Salpingoeca_infusionum_CHO",
    "Salpingoeca kvevrii" : "SKVE_Salpinogoeca_kvevrii_CHO",
    "Salpingoeca macrocollata" : "SMAC_Salpinogaeca_macrocollata_CHO",
    "Salpingoeca punica" : "SPUN_Salpinogaeca_punica_CHO",
    "Salpingoeca urceolata" : "SURC_Salpinogaeca_urceloata_CHO",
    "Savillea parva" : "SPAR_Savillea_parva_CHO",
    "Stephanoeca diplocostata.Australia" : "SDIP_Stephanoeca_diplocostata_Australia_CHO",
    "Stephanoeca diplocostata.France" : "SDIP_Stephanoeca_diplocostata_France_CHO"
    }

cladedict = {
    "CHO" : "Choanoflaggelata",
    "CNI" : "Cnidaria",
    "PLA" : "Placozoa",
    "FUN" : "Fungi",
    "TER" : "Teretosporea",
    "BIL" : "Bilateria",
    "POR" : "Porifera",
    "CTE" : "Ctenophora",
    "FIL" : "Filasterea",
    "VIR" : "Viridiplantae"
    }


proteins = ["HDAC1", "HDAC2", "RBBP4", "RBBP7", "SAP18", "SAP30", "SIN3"]

def main():
    filename=sys.argv[1]
    newname = filename.replace("out", '').split(".fa")[0].replace(".proteins", "")
    splitname = newname.split("_")
    if ("0" in splitname[0]) or ("1" in splitname[0]):
        newname = " ".join(newname.split("_")[1:])
    if len(newname.split("_")) <2:
        newname = namedict.get(newname)
    extrasplit = newname.split("_")
    for c in cladedict.keys():
        clade = cladedict.get(extrasplit[-1])
        break
    protein = sys.argv[2]
    speciesname = " ".join(newname.split("_")[1:-1])
    tree = ET.parse("./"+protein+"/"+sys.argv[1])
    root = tree.getroot()
    hit = 0
    if os.path.isfile('./blastdata.txt'):
        datareport = open("blastdata.txt", "a+")
    else:
        datareport = open("blastdata.txt", "w+")
        datareport.write("Protein\tClade\tSpecies\tE-Value\tScore\tQuerySeqLen\tQueryWindow\tHitWindow\tAlignmentLength\tIdentity\tCoverage\tGaps\n")
    for iteration in root.iter('Iteration'):
        transcript = iteration.find('Iteration_query-def').text
        querylength = iteration.find('Iteration_query-len').text
        for hit in iteration.iter('Hit'):
            hit = 1 
            for hsp in iteration.iter('Hsp'):
                alignlength = hsp.find('Hsp_align-len').text
                evalue = hsp.find('Hsp_evalue').text
                score = hsp.find('Hsp_score').text
                seq = hsp.find('Hsp_hseq').text
                querywindow = str(int(hsp.find('Hsp_query-to').text)-int(hsp.find('Hsp_query-from').text))
                hitwindow = str(int(hsp.find('Hsp_hit-to').text)-int(hsp.find('Hsp_hit-from').text))
                gaps = hsp.find('Hsp_gaps').text
                identity = hsp.find('Hsp_identity').text
                coverage = str((int(querywindow)/int(querylength))*100)
                if float(evalue) > 10e-5:
                    datareport.write(protein+"\t"+clade+"\t"+speciesname+"\t"+"Rejected!"+"\n")
                else: 
                    record = SeqRecord(Seq(seq, IUPAC.protein),id="",description=newname)
                    if os.path.isfile(newname+'_candidates.fasta'):
                        candidatesfile = open(protein+'_candidates.fasta', "a+")
                        print("say hi")
                    else:
                        candidatesfile = open(protein+'_candidates.fasta', "a+")
                    candidatesfile.write(">"+newname+"\n"+seq+"\n")
                    datareport.write(protein+"\t"+ clade+"\t"+ speciesname+"\t"+ evalue+"\t"+ score+"\t"+ querylength+"\t"+ querywindow+"\t"+ hitwindow+"\t"+ alignlength+"\t"+ identity+"\t"+ coverage+"\t"+ gaps+"\n")
                break
            break
        break #unbreak for all isoforms
        if hit == 0:
            datareport.write(protein+"\t"+clade+"\t"+speciesname+"\t"+"No Hit!")



if __name__ == "__main__":
    main()
