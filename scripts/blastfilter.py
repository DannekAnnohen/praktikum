import sys
import xml.etree.ElementTree as ET
import os

proteins = ["HDAC1", "HDAC2"],#"RBBP7", "SAP18", "SAP30", "SIN3"]


cladedict = {
    "CHO" : "Choanoflaggelata",
    "CNI" : "Cnidaria",
    "PLA" : "Placozoa",
    "FUN" : "Fungi",
    "TER" : "Teretosporea",
    "BIL" : "Bilateria",
    "POR" : "Porifera",
    "CTE" : "Ctenophora",
    "FIL" : "Filasterea",
    "VIR" : "Viridiplantae"
    }




def main():
  for pro in ["RBBP7", "RBBP4", "SAP18", "SAP30", "SIN3"]:
    protein = pro
    print("Now processing protein: "+protein)
    resultfiles = os.listdir("./Blastfull/"+protein+"/")
    for filename in resultfiles:
      print("Currently working on: "+filename)
      clade = cladedict.get(filename.split("_")[-3])
      tree = ET.parse("./Blastfull/"+protein+"/"+filename)
      root = tree.getroot()
      temp = filename.replace("_"+protein+"_results", "")
      proteome = open("./Proteome/"+temp)
      proteomdata = proteome.readlines()
      outputfile = open("./allcandidates/"+protein, "a+")
      if os.path.isfile('./blastdata_allvalid.txt'):
        datareport = open("blastdata.txt", "a+")
      else:
        datareport = open("blastdata_allvalid.txt", "w+")
        datareport.write("Protein\tClade\tSpecies\tE-Value\tScore\tQuerySeqLen\tQueryWindow\tHitWindow\tAlignmentLength\tIdentity\tCoverage\tGaps\n")
      for iteration in root.iter('Iteration'):
        transcript = iteration.find('Iteration_query-def').text
        querylength = iteration.find('Iteration_query-len').text
      hitno = 0
      for hit in iteration.iter('Hit'):
        hef = hit.find('Hit_def').text
        for hsp in iteration.iter('Hsp'):
          evalue = hsp.find('Hsp_evalue').text
          if float(evalue) > 10e-5:
            
            continue
          else:
            hitno += 1
            reading = 0
            seq = ""
            for line in proteomdata:
              if line[0] == ">":
                reading = 0 
              elif reading == 1:
                seq = seq+line.replace("\n", "")
              elif seq == "" and reading == 1: break
              if hef in line:
                reading = 1
            alignlength = hsp.find('Hsp_align-len').text
            score = hsp.find('Hsp_score').text
            querywindow = str(int(hsp.find('Hsp_query-to').text)-int(hsp.find('Hsp_query-from').text))
            hitwindow = str(int(hsp.find('Hsp_hit-to').text)-int(hsp.find('Hsp_hit-from').text))
            gaps = hsp.find('Hsp_gaps').text
            identity = hsp.find('Hsp_identity').text
            coverage = str((int(querywindow)/int(querylength))*100)
            #outputfile.write(protein+"_in_"+
            datareport.write(protein+"\t"+ clade+"\t"+ speciesname+"\t"+ evalue+"\t"+ score+"\t"+ querylength+"\t"+ querywindow+"\t"+ hitwindow+"\t"+ alignlength+"\t"+ identity+"\t"+ coverage+"\t"+ gaps+"\n")
            temp2 = filename.replace("_"+protein+"_results.fasta", "")
            identifier = protein+"__in__"+temp2+"_#"+str(hitno)
            outputfile.write(">"+identifier+"\n"+seq+"\n")
            break
          break
        break
        if hitno == 0: print("all rejected")


if __name__ == "__main__":
    main()
