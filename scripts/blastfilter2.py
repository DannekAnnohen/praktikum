import sys
import xml.etree.ElementTree as ET
import os

proteins = ["HDAC1", "HDAC2","RBBP7", "RBBP4", "SAP18", "SAP30", "SIN3"]


cladedict = {
    "CHO" : "Choanoflaggelata",
    "CNI" : "Cnidaria",
    "PLA" : "Placozoa",
    "FUN" : "Fungi",
    "TER" : "Teretosporea",
    "BIL" : "Bilateria",
    "POR" : "Porifera",
    "CTE" : "Ctenophora",
    "FIL" : "Filasterea",
    "VIR" : "Viridiplantae"
    }




def main():
 for pro in proteins:
  protein = pro
  resultfiles = os.listdir("./backwardsblastresults/"+protein+"/")
  for filename in resultfiles:
    species = filename.split("_genome")[0]
    for cla in cladedict.keys():
      if cla in filename:
        clade = cladedict.get(cla)
    speciesname = species.split("_")[1:-1]
    print("Currently working on: "+protein+" in "+species)
    tree = ET.parse("./backwardsblastresults/"+protein+"/"+filename)
    root = tree.getroot()
    genome = open("./Genomes/"+species+"_genome.fasta")
    genomedata = genome.readlines()
    outputfile = open("./backwardscandidates/"+protein, "a+")
    if os.path.isfile('./blastdata_backwards.txt'):
      datareport = open("blastdata_backwards.txt", "a+")
    else:
      datareport = open("blastdata_backwards.txt", "w+")
      datareport.write("Protein\tClade\tSpecies\tE-Value\tScore\tQuerySeqLen\tQueryWindow\tHitWindow\tAlignmentLength\tIdentity\tCoverage\tGaps\n")
    for iteration in root.iter('Iteration'):
      transcript = iteration.find('Iteration_query-def').text
      querylength = iteration.find('Iteration_query-len').text
    hitno = 0
    seq = ""
    for hit in iteration.iter('Hit'):
      hef = hit.find('Hit_def').text
      ho = hit.find('Hit_num').text
      for hsp in iteration.iter('Hsp'):
        hitno += 1
        seq = seq+hsp.find('Hsp_hseq').text.replace("\n", "")
        print(seq)
      identifier = protein+"__in__"+species+"_#"+str(hitno)
      outputfile.write(">"+identifier+"\n"+seq+"\n")
      break


def other1():
        #else:
          hitno += 1
          reading = 0
          seq = ""
          for line in genomedata:
            if line[0] == ">":
              reading = 0 
            elif reading == 1:
              seq = seq+line.replace("\n", "")
            elif seq == "" and reading == 1: break
            if hef in line:
              reading = 1
          alignlength = hsp.find('Hsp_align-len').text
          score = hsp.find('Hsp_score').text
          querywindow = str(int(hsp.find('Hsp_query-to').text)-int(hsp.find('Hsp_query-from').text))
          hitwindow = str(int(hsp.find('Hsp_hit-to').text)-int(hsp.find('Hsp_hit-from').text))
          gaps = hsp.find('Hsp_gaps').text
          identity = hsp.find('Hsp_identity').text
          coverage = str((int(querywindow)/int(querylength))*100)
          #outputfile.write(protein+"_in_"+
          datareport.write(protein+"\t"+ clade+"\t"+ speciesname+"\t"+ evalue+"\t"+ score+"\t"+ querylength+"\t"+ querywindow+"\t"+ hitwindow+"\t"+ alignlength+"\t"+ identity+"\t"+ coverage+"\t"+ gaps+"\n")
          temp2 = filename.replace("_"+protein+"_results.fasta", "")
          identifier = protein+"__in__"+temp2+"_#"+str(hitno)
          outputfile.write(">"+identifier+"\n"+seq+"\n")
#          break
#        break
#      break


if __name__ == "__main__":
    main()
